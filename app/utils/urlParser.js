export const urlParser = url => {
  if (process.env.NODE_ENV === 'development') {
    return `http://localhost:8000${url}`;
  }
  return url;
};

export default urlParser;
