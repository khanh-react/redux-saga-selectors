export const waitForAnimate = miliseconds =>
  new Promise(resolve => {
    setTimeout(() => resolve(), miliseconds);
  });
