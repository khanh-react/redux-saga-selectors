/**
 * Combine all reducers in this file and export the combined reducers.
 */
import { connectRouter } from 'connected-react-router';
import languageProviderReducer from 'containers/LanguageProvider/reducer';
import { combineReducers } from 'redux';
import history from 'utils/history';

import globalReducer from './containers/App/reducer';
import homeReducer from './containers/HomePage/reducer';
import othersReducer from './containers/OthersPage/reducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    global: globalReducer,
    language: languageProviderReducer,
    home: homeReducer,
    other: othersReducer,
    router: connectRouter(history),
    ...injectedReducers,
  });

  return rootReducer;
}
