import 'react-notifications/lib/notifications.css';

import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
*{
	margin: 0;
	padding: 0;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
  line-height: normal;
}

::-webkit-input-placeholder {
	color:#aaa;
	font-weight: 300;
}

::-moz-placeholder {
	color:#aaa;
	font-weight: 300;
}

:-ms-input-placeholder {
	color:#aaa;
	font-weight: 300;
}

input:-moz-placeholder {
	color:#aaa;
	font-weight: 300;
}

html,
body {
  box-sizing: border-box;
  height: 100%;
  margin: 0;
  font-family: "Barlow", Arial, sans-serif;
  color: #2F2F2F;
  font-weight: 300;
  overflow: hidden;
}

html.popup-opened #popup {
  position: fixed;
  width: 100%;
  height: 100vh;
  top: 0;
  display: flex;
}

html.popup-opened #popup .popup {
  display: flex;
}

body.report-opened #report-page {
  display: flex;
}

span, div {
  letter-spacing: normal;
}

.container {
  margin-top: 50px;
  width: 100%;
  height: calc(100% - 100px);
  overflow: scroll;
}

.container-fluid {
  padding-left: 100px;
  padding-right: 100px;
}

.notification-container {
  padding-top: 40px;
}

#app {
  width: 100%;
  height: 100%;
}

#popup .popup {
  width: 300px;
  top: 100px;
  left: calc(50% - 150px);
  flex-direction: column;
  background: #ffffff;
  border-radius: 8px;
  box-shadow: -5px 3px 20px 5px darkslategrey;
  position: absolute;
  display: none;
}

.popup .header {
  display: flex;
  height: 50px;
  border-bottom: 1px solid silver;
  padding: 6px;
  align-items: center;
  justify-content: center;
}

.popup .header .message {
  font-size: initial;
  font-weight: 900;
  font-family: cursive;
  color: green;
}

.popup .header .closer {
  position: absolute;
  right: 5px;
  cursor: pointer;
  font-size: medium;
  font-weight: 700;
}

.popup .body {
  margin-bottom: 50px;
  width: 100%;
}

.popup .content {
  padding: 10px;
  text-align: center;
}

.popup .footer {
  width: 100%;
  margin-bottom: 10px;
  position: absolute;
  bottom: 0;
  display: flex;
  justify-content: center;
}

button {
  cursor: pointer;
}

button.button {
  min-width: 60px;
  height: 30px;
}

button.button.primary {
  background-color: #28a0f9;
  border: 1px solid aqua;
  border-radius: 8px;
}

a {
  text-decoration-line: none;
  color: unset;
}

.mgr-5 {
  margin-right: 5px;
}

.as-btn:focus,
.as-btn:hover,
.as-btn:active {
  outline: none;
}

@media only screen and (max-width: 600px) {
  .container-fluid {
    padding-left: 20px;
    padding-right: 20px;
  }
}
`;

export default GlobalStyle;
