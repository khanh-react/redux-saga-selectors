import PropTypes from 'prop-types';
import React from 'react';

import ButtonStyles from './styles';

const Button = ({ size, text, type, onClick, disabled }) => (
  <ButtonStyles
    className={`btn-bordered ${size} ${type} ${disabled ? 'disabled' : ''}`}
    onClick={e => !disabled && onClick(e)}
  >
    {text}
  </ButtonStyles>
);

Button.propTypes = {
  size: PropTypes.string,
  text: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

export default Button;
