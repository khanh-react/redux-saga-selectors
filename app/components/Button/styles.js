import styled from 'styled-components';

const styles = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
  min-width: 70px;
  min-height: 30px;
  padding-top: 2px;
  box-shadow: rgb(40 160 249) 0px 0px 5px 0px;
  &.disabled {
    background-color: white !important;
    color: #000000 !important;
    opacity: 0.5;
    &:focus,
    &:active {
      outline: none;
    }
  }
  .btn-icon {
    padding: 0px;
    margin-top: 1px;
    &.mgr-5 {
      margin-right: 5px;
    }
  }

  // BUTTON SIZES:
  &.sm {
  }
  &.md {
    height: 30px;
  }
  &.lg {
  }

  &:hover {
    cursor: pointer;
  }

  // BUTTON FONT STYLES:
  &.bold {
    font-weight: 600;
    color: #616161;
  }

  // BUTTON TYPES:
  &.primary {
    border: none;
    background-color: #06dcf9;
    color: #ffffff;
    font-weight: bold;
  }

  &.warning {
    border: none;
    background-color: #ffa726;
    color: #ffffff;
  }

  &.default {
    border: 1px solid silver;
    background-color: unset;
    &:hover {
      background-color: #eeeeee;
    }
  }

  &.social {
    .button-wrapper {
      width: 220px;
      display: flex;
      align-items: center;
    }
  }

  &.btn-bordered {
    border-radius: 20px;
  }
`;

export default styles;
