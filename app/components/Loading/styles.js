import styled from 'styled-components';

const styles = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  .loading {
    width: 70px;
    height: 70px;
    background-color: white;
    border: 1px solid #06dcf9;
    border-radius: 15px;
    box-shadow: 2px 1px 10px 1px #06dcf9;
    background-repeat: no-repeat;
    background-size: contain;
    display: flex;
    align-items: center;
    justify-content: center;
    img {
      width: 100%;
    }
  }
}
`;

export default styles;
