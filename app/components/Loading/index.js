import LoadingIcon from 'images/loading.svg';
import PropTypes from 'prop-types';
import React from 'react';

import LoadingStyles from './styles';

export function Loading({ isLoading }) {
  if (isLoading) {
    return (
      <LoadingStyles>
        <div className="loading">
          <img src={LoadingIcon} alt="bestaff loading icon" />
        </div>
      </LoadingStyles>
    );
  }
  return null;
}

Loading.propTypes = {
  isLoading: PropTypes.bool,
};

export default Loading;
