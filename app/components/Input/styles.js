import styled from 'styled-components';

export const InputStyles = styled.input`
  width: 100%;
  height: 35px;
  border-radius: 20px;
  border: 1px solid silver;
  padding: 10px;
  &:focus,
  &:active {
    outline: none;
  }
  &.italic::placeholder {
    font-size: 13px;
    font-style: italic;
    display: flex;
    justify-content: flex-end;
    align-items: center;
  }
  &.no-border {
    border: none !important;
  }
`;

export const TextareaStyles = styled.textarea`
  width: 100%;
  height: 150px;
  border-radius: 20px;
  border: 1px solid silver;
  resize: none;
  padding: 10px;
  font-size: 16px;
  &:focus,
  &:active {
    outline: none;
  }
  &.italic::placeholder {
    font-size: 13px;
    font-style: italic;
    display: flex;
    justify-content: flex-end;
  }
  &.no-border {
    border: none;
  }
`;

export default InputStyles;
