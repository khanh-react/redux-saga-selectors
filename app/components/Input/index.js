import propTypes from 'prop-types';
import React from 'react';

import InputStyles, { TextareaStyles } from './styles';

const Input = ({
  className,
  placeholder,
  type = 'default',
  value,
  isNoBorder,
  onChange,
}) => {
  if (type === 'default')
    return (
      <InputStyles
        className={`${className} ${isNoBorder ? 'no-border' : ''}`}
        placeholder={placeholder}
        value={value}
        onChange={e => onChange(e.target.value)}
      />
    );
  return (
    <TextareaStyles
      className={`${className} ${isNoBorder ? 'no-border' : ''}`}
      placeholder={placeholder}
      value={value}
      onChange={e => onChange(e.target.value)}
    />
  );
};

Input.propTypes = {
  className: propTypes.string,
  placeholder: propTypes.string,
  type: propTypes.string,
  value: propTypes.string,
  isNoBorder: propTypes.bool,
  onChange: propTypes.func,
};

export default Input;
