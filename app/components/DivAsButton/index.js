import PropTypes from 'prop-types';
import React from 'react';

import DivAsButtonStyles from './styles';

const DivAsButton = ({ className, isHidden, onClick, children }) => {
  if (isHidden) return null;
  return (
    <DivAsButtonStyles
      className={`div-as-button ${className}`}
      onClick={e => onClick(e)}
      onKeyPress={e => onClick(e)}
      role="button"
      tabIndex="0"
    >
      {children}
    </DivAsButtonStyles>
  );
};

DivAsButton.propTypes = {
  className: PropTypes.string,
  isHidden: PropTypes.bool,
  onClick: PropTypes.func,
  children: PropTypes.any,
};

export default DivAsButton;
