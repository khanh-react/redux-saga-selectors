/* eslint-disable no-param-reassign */
import produce from 'immer';

import {
  CHANGE_VALUES_CONTACT_INFOS,
  CHECK_FORM_VALIDATION,
  CLOSE_CONTACT_FORM,
  DISABLE_DATA_NEXT,
  GET_EMPLOYEES,
  GET_EMPLOYEES_SUCCESS,
  RATE_EMPLOYEE,
  SUBMIT_PRE_SEND,
  SUBMIT_PRE_SEND_SUCCESS,
  SUBMIT_SEND_WITH_INFO,
  SUBMIT_SEND_WITH_INFO_SUCCESS,
  SUBMIT_SEND_WITHOUT_INFO,
  SUBMIT_SEND_WITHOUT_INFO_SUCCESS,
  UNRATE_EMPLOYEE,
  UPDATE_LOADING,
  UPDATE_RATING_VALUES,
} from './constants';

// Initial State
export const initialState = {
  data: [],
  loading: false,
  dataNext: true,
  preSend: false,
  currentEmployee: null,
  currentEmployeeIndex: null,
  ratingValues: {
    rating: null,
    letters: null,
    evaluates: [],
    images: [],
  },
  contactInfoValid: false,
  formType: 'MAIN',
  contactInfo: {
    fullname: '',
    phone: '',
    notes: '',
  },
};

/* eslint-disable default-case, no-param-reassign */
const HomeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_EMPLOYEES: {
        draft.loading = true;
        break;
      }

      case GET_EMPLOYEES_SUCCESS:
        draft.data = action.employees;
        draft.loading = false;
        break;

      case DISABLE_DATA_NEXT: {
        draft.dataNext = false;
        break;
      }

      case RATE_EMPLOYEE: {
        const employees = draft.data;
        const { key, value, index } = action.payload;
        const { currentEmployeeIndex } = draft;
        if (currentEmployeeIndex !== null) {
          if (currentEmployeeIndex !== index) {
            delete employees[currentEmployeeIndex].rating;
          }
        }
        employees[index][key] = value;
        draft.data = [...employees];
        draft.currentEmployeeIndex = index;
        break;
      }

      case UNRATE_EMPLOYEE: {
        draft.currentEmployeeIndex = null;
        const employees = draft.data;
        employees.forEach(emp => delete emp.rating);
        draft.data = [...employees];
        break;
      }

      case UPDATE_LOADING: {
        draft.loading = action.isLoading;
        break;
      }

      case UPDATE_RATING_VALUES: {
        const { key, value } = action.payload;
        draft.ratingValues[key] = value;
        break;
      }

      case SUBMIT_PRE_SEND: {
        draft.currentEmployeeIndex = action.index;
        if (draft.contactInfo.fullname && draft.contactInfo.phone) {
          draft.contactInfoValid = true;
        } else {
          draft.contactInfoValid = false;
        }
        break;
      }

      case SUBMIT_PRE_SEND_SUCCESS: {
        draft.preSend = true;
        break;
      }

      case SUBMIT_SEND_WITH_INFO: {
        const employees = draft.data;
        delete employees[draft.currentEmployeeIndex].rating;
        draft.data = [...employees];
        break;
      }

      case SUBMIT_SEND_WITHOUT_INFO_SUCCESS:
      case SUBMIT_SEND_WITH_INFO_SUCCESS: {
        draft.preSend = false;
        break;
      }

      case SUBMIT_SEND_WITHOUT_INFO: {
        const employees = draft.data;
        delete employees[draft.currentEmployeeIndex].rating;
        draft.data = [...employees];
        break;
      }

      case CHECK_FORM_VALIDATION: {
        const { contactInfo } = draft;
        if (contactInfo.fullname && contactInfo.phone) {
          draft.contactInfoValid = true;
        } else {
          draft.contactInfoValid = false;
        }
        break;
      }

      case CHANGE_VALUES_CONTACT_INFOS: {
        const { key, value } = action.payload;
        draft.contactInfo[key] = value;
        break;
      }

      case CLOSE_CONTACT_FORM: {
        draft.preSend = false;
        draft.contactInfo.notes = '';
        break;
      }
    }
  });

export default HomeReducer;
