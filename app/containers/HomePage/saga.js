import { call, delay, put, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';

import { doSendNotification, doUpdateLoading } from '../App/actions';
import {
  doDisableDataNext,
  doFetchEmployeesSucceeded,
  doSendWithInfoSuccess,
  doSendWithOutInfoSuccess,
  doSetPreSendSuccess,
} from './actions';
import { GET_EMPLOYEES, SUBMIT_PRE_SEND, SUBMIT_SEND_WITH_INFO, SUBMIT_SEND_WITHOUT_INFO } from './constants';

export function* getEmployees(params) {
  const { query } = params;
  const requestURL = `/api/employees?shift=${query}`;
  yield put(doUpdateLoading(true));
  // yield put(doSendNotification({ type: 'SUCCESS', message: 'asdadasdasd' }));
  try {
    const employees = yield call(request, requestURL);
    yield put(doFetchEmployeesSucceeded(employees));
    if (query === 'all') {
      yield put(doDisableDataNext());
    }
    yield put(doUpdateLoading(false));
  } catch (err) {
    yield put(doUpdateLoading(false));
    // eslint-disable-next-line no-console
    console.error('ERROR-1101');
  }
}

export function* submitSendRatingWithOutInfo() {
  yield put(doUpdateLoading(true));
  yield delay(1000);
  yield put(doUpdateLoading(false));
  yield put(doSendWithOutInfoSuccess());
  const noty = {
    type: 'SUCCESS',
    message: 'Trình Coffe chân thành cảm ơn đóng góp của quý khách !',
  };
  yield put(doSendNotification(noty));
}

export function* submitSendRatingWithInfo() {
  yield put(doUpdateLoading(true));
  yield delay(1000);
  yield put(doUpdateLoading(false));
  yield put(doSendWithInfoSuccess());
  const noty = {
    type: 'SUCCESS',
    message: 'Trình Coffe chân thành cảm ơn đóng góp của quý khách !',
  };
  yield put(doSendNotification(noty));
}

export function* submitPreSend() {
  yield put(doUpdateLoading(true));
  yield delay(1000);
  yield put(doUpdateLoading(false));
  yield put(doSetPreSendSuccess());
}

export default function* homeSaga() {
  yield takeLatest(GET_EMPLOYEES, getEmployees);
  yield takeLatest(SUBMIT_SEND_WITHOUT_INFO, submitSendRatingWithOutInfo);
  yield takeLatest(SUBMIT_SEND_WITH_INFO, submitSendRatingWithInfo);
  yield takeLatest(SUBMIT_PRE_SEND, submitPreSend);
}
