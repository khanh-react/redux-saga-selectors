import {
  CHANGE_VALUES_CONTACT_INFOS,
  CHECK_FORM_VALIDATION,
  CLOSE_CONTACT_FORM,
  DISABLE_DATA_NEXT,
  GET_EMPLOYEES,
  GET_EMPLOYEES_SUCCESS,
  RATE_EMPLOYEE,
  SUBMIT_PRE_SEND,
  SUBMIT_PRE_SEND_SUCCESS,
  SUBMIT_SEND_WITH_INFO,
  SUBMIT_SEND_WITH_INFO_SUCCESS,
  SUBMIT_SEND_WITHOUT_INFO,
  SUBMIT_SEND_WITHOUT_INFO_SUCCESS,
  UNRATE_EMPLOYEE,
  UPDATE_LOADING,
  UPDATE_RATING_VALUES,
} from './constants';

export function doFetchEmployees(query) {
  return {
    type: GET_EMPLOYEES,
    query,
  };
}

export function doFetchEmployeesSucceeded(employees) {
  return {
    type: GET_EMPLOYEES_SUCCESS,
    employees,
  };
}

export function doRateEmployee(payload) {
  return {
    type: RATE_EMPLOYEE,
    payload,
  };
}

export function doUpdateRatingValue(payload) {
  return {
    type: UPDATE_RATING_VALUES,
    payload,
  };
}

export function doUnRateEmployee() {
  return {
    type: UNRATE_EMPLOYEE,
  };
}

export function doDisableDataNext() {
  return {
    type: DISABLE_DATA_NEXT,
  };
}

export function doLoading(isLoading) {
  return {
    type: UPDATE_LOADING,
    isLoading,
  };
}

export function doSubmitPreSend(employeeIndex) {
  return {
    type: SUBMIT_PRE_SEND,
    index: employeeIndex,
  };
}

export function doSetPreSendSuccess() {
  return {
    type: SUBMIT_PRE_SEND_SUCCESS,
  };
}

export function doSend(sendingType) {
  return {
    type: sendingType === 1 ? SUBMIT_SEND_WITH_INFO : SUBMIT_SEND_WITHOUT_INFO,
    sendingType,
  };
}

export function doSendWithOutInfoSuccess() {
  return {
    type: SUBMIT_SEND_WITHOUT_INFO_SUCCESS,
  };
}

export function doSendWithInfoSuccess() {
  return {
    type: SUBMIT_SEND_WITH_INFO_SUCCESS,
  };
}
export function doCheckValidation() {
  return {
    type: CHECK_FORM_VALIDATION,
  };
}

export function doCloseContactForm() {
  return {
    type: CLOSE_CONTACT_FORM,
  };
}

export function doChangeContactInfos(payload) {
  return {
    type: CHANGE_VALUES_CONTACT_INFOS,
    payload,
  };
}

