import PropTypes from 'prop-types';
import React, { memo, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import { doUpdateLoading, doWaitForAnimate } from '../App/actions';
import Slogan from '../App/components/Slogan';
import WifiPassword from '../App/components/WifiPassword';
import {
  doChangeContactInfos,
  doCheckValidation,
  doCloseContactForm,
  doFetchEmployees,
  doRateEmployee,
  doSend,
  doSubmitPreSend,
  doUnRateEmployee,
} from './actions';
import EmployeeList from './components/EmployeeList';
import Loadmore from './components/Loadmore';
import PreSendForm from './components/PreSendForm';
import { HOME_PAGE_SLOGAN } from './constants';
import reducer from './reducer';
import saga from './saga';
import {
  makeSelectContactFormValid,
  makeSelectContactInfo,
  makeSelectCurrentEmployeeIndex,
  makeSelectEmployees,
  makeSelectEmployeesNext,
  makeSelectFormType,
  makeSelectLoading,
  makeSelectPreSend,
} from './selectors';
import HomePageStyles from './styles';

// import { doSubmitPreSend } from '../OthersPage/actions';
const key = 'home';

function HomePage({
  employees,
  currentEmployeeIndex,
  isLoading = false,
  isPreSending,
  isValid,
  next = true,
  form,
  contactInfo,
  onGetEmployees,
  onChangeContactInfo,
  onCheckValidation,
  onCloseContactForm,
  onUnRate,
  onRatingChange,
  onPreSend,
  onSend,
}) {
  // eslint-disable-next-line no-console
  console.log('::: HOME PAGE RENDERED :::');
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const [list] = useState([employees]);

  useEffect(() => {
    onGetEmployees();
  }, [list]);

  return (
    <HomePageStyles className="container container-fluid">
      <WifiPassword />
      <Slogan slogan={HOME_PAGE_SLOGAN} />
      <EmployeeList
        isVisible={form === 'MAIN' && !isPreSending}
        employees={employees}
        currentEmployeeIndex={currentEmployeeIndex}
        setPreSend={employeeIndex => onPreSend(employeeIndex)}
        onGetClickOutside={() => onUnRate()}
        onGetRatingPointValue={({ value, index }) =>
          onRatingChange({ key: 'rating', value, index })
        }
        onGetRatingEvaluateValue={({ value, index }) =>
          onRatingChange({ key: 'evaluate', value, index })
        }
        onGetRatingLettersValue={({ value, index }) =>
          onRatingChange({ key: 'letters', value, index })
        }
      />
      <PreSendForm
        isVisible={isPreSending}
        isValid={isValid}
        item={employees[currentEmployeeIndex]}
        contactInfo={contactInfo}
        onSend={type => onSend(type)}
        onChange={e => onChangeContactInfo(e) && onCheckValidation()}
        onClose={() => onCloseContactForm()}
      />
      <Loadmore
        isVisible={next && !isPreSending}
        isLoading={isLoading}
        onClick={() => onGetEmployees('all')}
      />
    </HomePageStyles>
  );
}

// Retrieve data from store as props
const mapStateToProps = createStructuredSelector({
  employees: makeSelectEmployees(),
  currentEmployeeIndex: makeSelectCurrentEmployeeIndex(),
  isLoading: makeSelectLoading(),
  isPreSending: makeSelectPreSend(),
  isValid: makeSelectContactFormValid(),
  next: makeSelectEmployeesNext(),
  form: makeSelectFormType(),
  contactInfo: makeSelectContactInfo(),
});

const mapDispatchToProps = dispatch => ({
  onUpdateLoading: isLoading => dispatch(doUpdateLoading(isLoading)),
  onGetEmployees: query => dispatch(doFetchEmployees(query)),
  // onSetPreSend: employeeIndex => dispatch(doSetPreSend(employeeIndex)),
  onRatingChange: payload => dispatch(doRateEmployee(payload)),
  onUnRate: () => dispatch(doUnRateEmployee()),
  onChangeContactInfo: payload => dispatch(doChangeContactInfos(payload)),
  onCheckValidation: () => dispatch(doCheckValidation()),
  onCloseContactForm: () => dispatch(doCloseContactForm()),
  onSetWaitForAnimate: () => dispatch(doWaitForAnimate()),
  onPreSend: type => dispatch(doSubmitPreSend(type)),
  onSend: type => dispatch(doSend(type)),
});

HomePage.propTypes = {
  employees: PropTypes.array,
  currentEmployeeIndex: PropTypes.number,
  isLoading: PropTypes.bool,
  next: PropTypes.bool,
  form: PropTypes.string,
  contactInfo: PropTypes.object,
  isPreSending: PropTypes.bool,
  isValid: PropTypes.bool,
  // onUpdateLoading: PropTypes.func,
  // onSetWaitForAnimate: PropTypes.func,
  onGetEmployees: PropTypes.func,
  onRatingChange: PropTypes.func,
  onUnRate: PropTypes.func,
  onChangeContactInfo: PropTypes.func,
  onCheckValidation: PropTypes.func,
  onCloseContactForm: PropTypes.func,
  onPreSend: PropTypes.func,
  onSend: PropTypes.func,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
