import { createSelector } from 'reselect';

import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectEmployees = () =>
  createSelector(
    selectHome,
    state => state.data,
  );

const makeSelectLoading = () =>
  createSelector(
    selectHome,
    state => state.loading,
  );

const makeSelectEmployeesNext = () =>
  createSelector(
    selectHome,
    state => state.dataNext,
  );

const makeSelectFormType = () =>
  createSelector(
    selectHome,
    state => state.formType,
  );

const makeSelectPreSend = () =>
  createSelector(
    selectHome,
    state => state.preSend,
  );

const makeSelectCurrentEmployeeIndex = () =>
  createSelector(
    selectHome,
    state => state.currentEmployeeIndex,
  );

const makeSelectContactInfo = () =>
  createSelector(
    selectHome,
    state => state.contactInfo,
  );

const makeSelectContactFormValid = () =>
  createSelector(
    selectHome,
    state => state.contactInfoValid,
  );

export {
  selectHome,
  makeSelectEmployees,
  makeSelectLoading,
  makeSelectEmployeesNext,
  makeSelectFormType,
  makeSelectPreSend,
  makeSelectCurrentEmployeeIndex,
  makeSelectContactInfo,
  makeSelectContactFormValid,
};
