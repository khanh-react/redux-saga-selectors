import styled from 'styled-components';

const styles = styled.div`
  #dot {
    display: inline-block;
    width: 7px;
    height: 7px;
    border-radius: 50%;
    margin-left: 9px;
    margin-right: 9px;
    border: 1px solid;
  }

  .btn-send-wrapper {
    display: flex;
    justify-content: center;
    div {
      width: 85px;
    }
  }
`;

export default styles;
