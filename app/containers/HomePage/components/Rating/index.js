import PropTypes from 'prop-types';
import React from 'react';
import ReactRating from 'react-star-ratings';

export const Rating = ({ rating, onChange }) => {
  if (!rating)
    return (
      <ReactRating
        svgIconPath="M49.728,17.966c-0.126-0.388-0.476-0.661-0.884-0.688L32.16,16.146L25.928,0.627C25.775,0.249,25.408,0,25,0
    s-0.776,0.249-0.928,0.627L17.84,16.146L1.156,17.277c-0.407,0.027-0.757,0.3-0.883,0.688c-0.126,0.389-0.003,0.814,0.31,1.077
    l12.832,10.722L9.335,45.98c-0.1,0.396,0.052,0.813,0.382,1.053c0.331,0.24,0.773,0.255,1.12,0.038L25,38.181l14.164,8.891
    c0.162,0.103,0.347,0.153,0.531,0.153c0.207,0,0.413-0.064,0.588-0.191c0.33-0.239,0.481-0.656,0.382-1.053l-4.08-16.216
    l12.833-10.722C49.731,18.78,49.854,18.354,49.728,17.966z M34.82,28.632c-0.294,0.246-0.422,0.64-0.328,1.012l3.626,14.41
    l-12.587-7.9c-0.324-0.205-0.738-0.205-1.063,0l-12.586,7.9l3.625-14.41c0.094-0.372-0.034-0.766-0.329-1.012L3.776,19.104
    l14.826-1.005c0.383-0.026,0.717-0.269,0.86-0.625L25,3.684l5.538,13.79c0.143,0.356,0.478,0.599,0.86,0.625l14.826,1.005
    L34.82,28.632z"
        // starEmptyColor="#f2ccf3"
        starHoverColor="rgb(213, 85, 215)"
        starRatedColor="rgb(213, 85, 215)"
        rating={rating}
        starDimension="32px"
        starSpacing="5px"
        numberOfStars={5}
        name="rating"
        isAggregateRating
        isSelectable
        changeRating={rate => onChange(rate)}
      />
    );
  return (
    <ReactRating
      starHoverColor="rgb(213, 85, 215)"
      starEmptyColor="#f2ccf3"
      starRatedColor="rgb(213, 85, 215)"
      rating={rating}
      starDimension="32px"
      starSpacing="5px"
      numberOfStars={5}
      name="rating"
      isAggregateRating
      isSelectable
      changeRating={rate => onChange(rate)}
    />
  );
};

Rating.propTypes = {
  rating: PropTypes.number,
  onChange: PropTypes.func,
};

export default Rating;
