import PropTypes from 'prop-types';
import React from 'react';

import LoadmoreStyles from './styles';

export const Loadmore = ({ isVisible, isLoading, onClick }) => {
  // if (isLoading) return <Loading isLoading />;
  if (isVisible)
    return (
      <LoadmoreStyles onClick={e => onClick(e)}>
        <span>Xem tất cả nhân sự của các ca trước đó</span>
      </LoadmoreStyles>
    );
  return null;
};

Loadmore.propTypes = {
  onClick: PropTypes.func,
  isVisible: PropTypes.bool,
  isLoading: PropTypes.bool,
};

export default Loadmore;
