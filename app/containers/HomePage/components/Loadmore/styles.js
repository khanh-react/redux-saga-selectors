import styled from 'styled-components';

export const styles = styled.div`
  text-align: center;
  span {
    padding-bottom: 1px;
    font-style: italic;
    font-size: 14px;
    display: inline-block;
    border-bottom: 1px solid;
    cursor: pointer;
  }
`;

export default styles;
