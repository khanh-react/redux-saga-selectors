import styled from 'styled-components';

const styles = styled.div`
  display: flex;
  flex-direction: column;
  padding: 15px 20px 15px 20px;
  flex-direction: column;
  margin-bottom: 20px;
  border-radius: 20px;
  background-color: #ffffff;
  border: 2px solid rgb(40, 160, 249);
  box-shadow: rgb(40 160 249) 0px 0px 5px 0px;
  position: relative;
  input {
    margin-bottom: 10px;
  }
  .row {
    font-family: inherit;
    font-style: italic;
    font-size: 12px;
    letter-spacing: normal;
    font-weight: normal;
    .fullname {
      color: rgb(57, 132, 198);
      font-weight: bold;
    }
    .contact {
      color: rgb(57, 132, 198);
    }
    &.info-contact {
      margin-top: 10px;
      margin-bottom: 10px;
    }
    .other {
      height: 150px;
    }
  }
  .foot {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-top: 10px;
    .btn-bordered {
      width: 90px;
      margin-bottom: 15px;
      width: 90px;
    }
    .skip-complete {
      text-decoration-line: underline;
    }
  }
  .closer {
    width: 30px;
    display: flex;
    height: 30px;
    border: 1px solid #28a0f9;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    position: absolute;
    right: -8px;
    top: -8px;
    background-color: #ffffff;
    color: #28a0f9;
    font-weight: bolder;
    cursor: pointer;
  }
`;

export default styles;
