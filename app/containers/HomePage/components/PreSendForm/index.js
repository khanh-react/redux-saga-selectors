import Button from 'components/Button';
import DivAsButton from 'components/DivAsButton';
import Input from 'components/Input';
import propTypes from 'prop-types';
import React from 'react';

import PreSendFormStyles from './styles';

export const PreSendForm = ({
  isVisible,
  isValid,
  item,
  contactInfo,
  onSend,
  onChange,
  onClose,
}) => {
  if (isVisible)
    return (
      <PreSendFormStyles>
        <DivAsButton className="closer" onClick={() => onClose()}>
          X
        </DivAsButton>
        <div className="row">
          Bạn đã đánh giá nhân sự{' '}
          <span className="fullname">{item.name}</span>.
        </div>
        <div className="row">
          Để lại thông tin liên lạc của bạn để chúng tôi có thể{' '}
          <span className="contact">liên hệ với bạn khi cần thiết </span>
          hoặc góp ý thêm các vấn đề khác nếu cần bạn nhé! Đội ngũ chúng tôi cam
          kết ghi nhận và xử lý tất cả các vấn đề mà khách hàng quan tâm
        </div>
        <div className="row info-contact">THÔNG TIN LIÊN HỆ</div>
        <div className="form">
          <Input
            className="italic"
            placeholder="Họ và tên *"
            value={contactInfo.fullname}
            onChange={value => onChange({ key: 'fullname', value })}
          />
          <Input
            className="italic"
            placeholder="Di động *"
            value={contactInfo.phone}
            onChange={value => onChange({ key: 'phone', value })}
          />
          <Input
            type="textarea"
            className="italic other"
            placeholder="Ý kiến khác"
            value={contactInfo.notes}
            onChange={value => onChange({ key: 'notes', value })}
          />
        </div>
        <div className="row foot">
          <Button
            type="primary"
            text="Hoàn tất"
            onClick={() => onSend(1)}
            disabled={!isValid}
          />
          <DivAsButton className="skip-complete" onClick={() => onSend(0)}>
            Bỏ qua và hoàn tất
          </DivAsButton>
        </div>
      </PreSendFormStyles>
    );
  return null;
};

PreSendForm.propTypes = {
  isVisible: propTypes.bool,
  isValid: propTypes.bool,
  item: propTypes.object,
  contactInfo: propTypes.object,
  onSend: propTypes.func,
  onChange: propTypes.func,
  onClose: propTypes.func,
};

export default PreSendForm;
