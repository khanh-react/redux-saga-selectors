import styled from 'styled-components';

const styles = styled.div`
  position: absolute;
  .carousel div {
    width: 100%;
    height: 200px;
    background: #000000;
    border-radius: 20px;
  }

  .options {
    position: absolute;
    width: 100%;
    height: 37px;
    bottom: 0;
    display: flex;
    align-items: center;
    z-index: 10;
  }

  .delete {
    margin-left: auto;
    margin-right: 10px;
    background-color: white;
    color: black;
    font-size: 10px;
    font-weight: bold;
    padding: 5px 10px 5px 10px;
    border-radius: 10px;
    cursor: pointer;
    position: absolute;
    bottom: 10px;
    right: 0;
    z-index: 10;
  }

  .close {
    color: #ffffff;
    font-size: 10px;
    font-weight: bold;
    margin-left: 10px;
    cursor: pointer;
    position: absolute;
    bottom: 5px;
    padding: 10px;
    z-index: 10;
  }
`;

export default styles;
