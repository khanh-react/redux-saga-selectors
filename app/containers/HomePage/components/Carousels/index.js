import 'react-responsive-carousel/lib/styles/carousel.min.css';

import PropTypes from 'prop-types';
import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import $url from 'utils/urlParser';

import CarouselStyles from './styles';

export class BeCarousel extends React.Component {
  componentDidMount() {}

  render() {
    return (
      <CarouselStyles>
        <Carousel className="carousel" showThumbs={false}>
          <img src={$url('/images/draft1.jpg')} />
          <img src={$url('/images/draft1.jpg')} />
          <img src={$url('/images/draft1.jpg')} />
          <img src={$url('/images/draft1.jpg')} />
          <img src={$url('/images/draft1.jpg')} />
          <img src={$url('/images/draft1.jpg')} />
        </Carousel>
        <span className="close" onClick={() => this.props.onClose()}>
          ĐÓNG
        </span>
        <span className="delete">XÓA</span>
      </CarouselStyles>
    );
  }
}

BeCarousel.propTypes = {
  onClose: PropTypes.func,
};

export default BeCarousel;
