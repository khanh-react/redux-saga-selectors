import styled from 'styled-components';

const styles = styled.div`
  display: block;
  padding-left: 10px;
  padding-right: 10px;
  .tag {
    background-color: #ffffff;
    margin-right: 5px;
    margin-bottom: 5px;
    border: 1px solid silver;
    border-radius: 10px;
    padding: 6px 10px;
    font-size: 11px;
    display: inline-block;
    cursor: pointer;
  }

  .tag.active {
    border: 1px solid rgb(213, 85, 215);
    color: rgb(213, 85, 215);
  }
`;

export default styles;
