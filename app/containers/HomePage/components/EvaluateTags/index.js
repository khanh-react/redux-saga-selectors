import DivAsButton from 'components/DivAsButton';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import ratingData from 'static/rating_data_default.json';

import EvaluateTagStyles from './styles';

const EvaluateTags = ({
  className,
  isVisible,
  evaluateKey,
  rating,
  onChange,
}) => {
  const dataRatingStaff = ratingData.find(e => e.key === evaluateKey);
  const [rates] = useState(dataRatingStaff);
  const [voted, setVoted] = useState({ rate: 0, values: [] });

  const onVote = item => {
    if (voted.rate !== rating) {
      voted.values = [];
      voted.rate = rating;
    }
    if (voted.values.includes(item)) {
      const index = voted.values.findIndex(e => item === e);
      voted.values.splice(index, 1);
    } else {
      voted.values.push(item);
    }
    setVoted({ ...voted });
    onChange(voted);
  };

  const isActive = item => {
    if (voted.values.includes(item)) {
      return 'active';
    }
    return '';
  };

  if (isVisible && rating) {
    return (
      <EvaluateTagStyles className={className}>
        {rates.options.map(option => (
          <DivAsButton
            key={option.id}
            className={`as-btn tag ${isActive(option.ratings[rating])}`}
            onClick={() => onVote(option.ratings[rating])}
          >
            {option.ratings[rating]}
          </DivAsButton>
        ))}
      </EvaluateTagStyles>
    );
  }
  return null;
};

EvaluateTags.propTypes = {
  className: PropTypes.string,
  evaluateKey: PropTypes.string,
  isVisible: PropTypes.bool,
  rating: PropTypes.number,
  // getValuateValues: PropTypes.array,
  onChange: PropTypes.func,
};

export default EvaluateTags;
