/* eslint-disable jsx-a11y/alt-text */
import 'react-responsive-carousel/lib/styles/carousel.min.css';

import DivAsButton from 'components/DivAsButton';
import Input from 'components/Input';
import UploadIcon from 'images/add-images.png';
import PropTypes from 'prop-types';
import React from 'react';
import $url from 'utils/urlParser';

import Carousel from '../Carousels';
import ImagesLetterStyles from './styles';

export class ImagesLetters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // letters: '',
      lettersLength: 0,
      carouselOpened: false,
    };
  }

  componentDidMount() {}

  onLettersChange(value) {
    // console.log('value', value, value.length);
    this.setState({
      lettersLength: value.length,
    });
    this.props.onLettersChange(value);
  }

  onOpenCarousel() {
    this.setState({
      carouselOpened: true,
    });
  }

  onCloseCarousel() {
    this.setState({
      carouselOpened: false,
    });
  }

  render() {
    const { className, rating, isVisible, onGetImagesChange } = this.props;
    if (rating && isVisible) {
      return (
        <ImagesLetterStyles className={className}>
          <h6>
            "Vui lòng cho chúng tôi bất kỳ gợi ý để cải thiện những điều này!"
          </h6>
          <div className="box">
            <div className="uploaderWraper">
              <div className="uploader">
                <img src={UploadIcon} />
              </div>
              <label htmlFor="file-uploader">
                <input
                  type="file"
                  id="file-uploader"
                  multiple
                  onChange={() => onGetImagesChange()}
                />
              </label>
              {/* <span>Thêm hình ảnh (0/5)</span> */}
              <DivAsButton
                className="listImages"
                onClick={() => this.onOpenCarousel()}
              >
                <img src={$url('/images/draft1.jpg')} />
                <img src={$url('/images/draft1.jpg')} />
                <img src={$url('/images/draft1.jpg')} />
                <img src={$url('/images/draft1.jpg')} />
              </DivAsButton>
            </div>
            <Input
              id="letters"
              className="letters"
              type="textarea"
              placeholder="Viết đánh giá của bạn tại đây"
              onChange={value => this.onLettersChange(value)}
              isNoBorder
            />
            <span className="lettersMaxLength">
              {this.state.lettersLength}/500
          </span>
            {this.state.carouselOpened && (
              <Carousel onClose={() => this.onCloseCarousel()} />
            )}
          </div>
        </ImagesLetterStyles>
      );
    }
    return null;
  }
}

ImagesLetters.propTypes = {
  className: PropTypes.string,
  rating: PropTypes.number,
  isVisible: PropTypes.bool,
  onLettersChange: PropTypes.func,
  onGetImagesChange: PropTypes.func,
};

export default ImagesLetters;
