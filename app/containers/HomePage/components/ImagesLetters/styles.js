import styled from 'styled-components';

const styles = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 10px 10px 10px;
  h6 {
    opacity: 0.6;
    font-weight: normal;
    margin-top: 10px;
    margin-bottom: 8px;
  }

  .box {
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 200px;
    border: 1px solid silver;
    border-radius: 20px;
    position: relative;
  }

  .uploaderWraper {
    margin: 10px;
    border-bottom: 1px solid silver;
    padding-bottom: 5px;
    height: 35px;
    display: flex;
    align-items: center;
    position: relative;
  }

  .uploaderWraper input[type='file'] {
    width: 32px;
    height: 32px;
    position: absolute;
    opacity: 0;
    top: 0;
    left: 0;
  }

  .listImages {
    width: calc(100% - 50px);
    height: 100%;
    margin-left: 10px;
    display: inline-block;
    overflow: hidden;
    cursor: pointer;
  }

  .listImages img {
    height: 100%;
    margin-left: 2px;
    border: 1px solid black;
    border-radius: 5px;
  }

  .uploaderWraper span {
    font-size: x-small;
    margin-left: 10px;
  }

  .uploaderWraper input {
    cursor: pointer;
  }

  .uploader {
    width: 32px;
    height: 32px;
    background-image: url('/images/add-images.png');
    cursor: pointer;
  }

  .letters {
    height: 145px;
    position: relative;
    display: flex;
    padding: 0px 10px 10px 10px;
    font-size: larger;
  }

  .letters::placeholder {
    font-size: 14px;
  }

  .lettersMaxLength {
    text-align: right;
    font-size: x-small;
    position: absolute;
    bottom: 5px;
    right: 10px;
  }

  [contentEditable='true']:empty:not(:focus):before {
    content: attr(data-text);
    opacity: 0.5;
  }

  [contentEditable]:focus,
  [contentEditable]:active {
    border: none;
    outline: none;
  }
`;
export default styles;
