import Button from 'components/Button';
import propTypes from 'prop-types';
import React from 'react';
import $url from 'utils/urlParser';

import EvaluateTags from '../EvaluateTags';
import ImagesLetters from '../ImagesLetters';
import Rating from '../Rating';

const Employee = ({
  info,
  index,
  onGetRatingPointValue,
  setPreSend,
  currentEmployeeIndex,
  onGetLettersValue,
  onGetRatingEvaluateValue,
}) => {
  const isRating = index === currentEmployeeIndex;
  return (
    <div className={`employee ${isRating ? 'rated' : ''}`} key={`${info.id}`}>
      <div className="default">
        <div className="avatar">
          <img src={`${$url('/images/sample-avatar.png')}`} alt="" />
          <span className={`onlineStatus ${info.status}`} />
        </div>
        <div className="info">
          <div className="infoName">{info.name}</div>
          <div className="infoRating">
            <Rating
              rating={info.rating}
              employeeIndex={index}
              onChange={value => onGetRatingPointValue(value)}
            />
          </div>
          <div className="role">
            {info.role} {info.role && <span id="dot" />} {info.ca}
          </div>
        </div>
      </div>
      <EvaluateTags
        evaluateKey="STAFF"
        rating={info.rating}
        isVisible={isRating}
        onChange={value => onGetRatingEvaluateValue(value)}
      />
      <ImagesLetters
        rating={info.rating}
        isVisible={isRating}
        onLettersChange={value => onGetLettersValue(value)}
      />
      {/* onGetImagesChange={() => } */}
      {info.rating && isRating && (
        <div className="btn-send-wrapper">
          <Button
            type="primary"
            size="md"
            text="Gửi"
            onClick={() => {
              if (!info.isPreSending) {
                return setPreSend(index);
              }
              return null;
            }}
          />
        </div>
      )}
    </div>
  );
};

Employee.propTypes = {
  info: propTypes.object,
  index: propTypes.number,
  currentEmployeeIndex: propTypes.number,
  onGetRatingPointValue: propTypes.func,
  setPreSend: propTypes.func,
  onGetLettersValue: propTypes.func,
  onGetRatingEvaluateValue: propTypes.func,
};

export default Employee;
