import styled from 'styled-components';

const styles = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);

  .employee {
    display: flex;
    flex-direction: column;
    margin-right: 20px;
    border-radius: 20px;
    margin-bottom: 10px;
    &.rated {
      background-color: #ffffff;
      border: 2px solid rgb(40, 160, 249);
      box-shadow: rgb(40 160 249) 0px 0px 5px 0px;
      padding-bottom: 10px;
      .default {
        background-color: unset;
        margin-bottom: unset;
      }
      .avatar img {
        border: 3px solid #28a0f9;
      }
      .infoName {
        color: #2196f3;
      }
      .role {
        color: rgb(40, 160, 249);
      }
      #dot {
        background: #7ac6fb;
      }
    }
  }

  .default {
    display: flex;
    align-items: center;
    border-radius: 20px;
    padding: 10px;
    margin-bottom: 10px;
    background-color: #f5f5f5;
  }

  .evaluate {
    display: flex;
  }

  .uploadForm {
    background-color: red;
  }

  .avatar {
    width: 80px;
    height: 80px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
      img {
        border: 3px solid #bdbdbd;
        border-radius: 50%;
        padding: 2px;
        width: 100%;
        height: 100%;
        object-fit: contain;
      }
    }
  }

  .info {
    margin-left: 10px;
    display: flex;
    flex-direction: column;
  }

  .infoName {
    text-transform: uppercase;
    font-weight: 700;
    color: #333333;
    opacity: 0.9;
    font-weight: bold;
    font-style: normal;
    line-height: 1.2;
  }

  .role {
    font-size: small;
    color: #333333;
    opacity: 0.8;
    text-transform: uppercase;
    margin-top: 5px;
    font-size: 11px;
    letter-spacing: normal;
    font-weight: normal;
    font-style: normal;
  }

  .onlineStatus {
    width: 10px;
    height: 10px;
    position: absolute;
    right: 0;
    border-radius: 50%;
    top: 19px;
    &.online {
      background: #31a24c;
    }
    &.offline {
      background: #c1c1c1;
      border: 1px solid;
    }
  }

  @media only screen and (max-width: 600px) {
    display: flex;
    flex-direction: column;
    .employee {
      margin-right: 0px;
    }
  }
`;

export default styles;
