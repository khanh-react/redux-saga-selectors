import PropTypes from 'prop-types';
import React from 'react';
import withOnClickOutside from 'react-onclickoutside';

import Employee from './Employee';
import EmployeeStyles from './styles';

// import Rating from 'react-star-ratings';
export function EmployeeList({
  employees,
  currentEmployeeIndex,
  onGetRatingEvaluateValue,
  onGetRatingPointValue,
  onGetRatingLettersValue,
  setPreSend,
  isVisible,
  onGetClickOutside,
}) {
  EmployeeList.handleClickOutside = () => onGetClickOutside();

  if (isVisible)
    return (
      <EmployeeStyles className="employees">
        {employees.map((item, index) => (
          <Employee
            key={item.id}
            info={item}
            index={index}
            currentEmployeeIndex={currentEmployeeIndex}
            setPreSend={e => setPreSend(e)}
            onGetRatingPointValue={value =>
              onGetRatingPointValue({ value, index })
            }
            onGetRatingEvaluateValue={value =>
              onGetRatingEvaluateValue({ value, index })
            }
            onGetLettersValue={value =>
              onGetRatingLettersValue({ value, index })
            }
          />
        ))}
      </EmployeeStyles>
    );
  return null;
}

EmployeeList.propTypes = {
  employees: PropTypes.array,
  currentEmployeeIndex: PropTypes.number,
  setPreSend: PropTypes.func,
  isVisible: PropTypes.bool,
  onGetClickOutside: PropTypes.func,
  onGetRatingPointValue: PropTypes.func,
  onGetRatingEvaluateValue: PropTypes.func,
  onGetRatingLettersValue: PropTypes.func,
};

const clickOutsideConfig = {
  handleClickOutside: () => EmployeeList.handleClickOutside,
};

export default withOnClickOutside(EmployeeList, clickOutsideConfig);
