import { createSelector } from 'reselect';

const selectRouter = state => state.router;
const selectGlobalState = state => state.global;

const makeSelectLocation = () =>
  createSelector(
    selectRouter,
    routerState => routerState.location,
  );

const makeSelectLoading = () =>
  createSelector(
    selectGlobalState,
    state => state.isLoading,
  );

export { makeSelectLocation, makeSelectLoading };
