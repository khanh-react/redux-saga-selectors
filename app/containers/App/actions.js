import { NOTIFICATION_TYPE, SEND_NOTIFICATION, UPDATE_APP_LOADING, WAIT_FOR_ANIMATE } from './constants';

export function doSendNotification({
  type = NOTIFICATION_TYPE,
  message,
  title = '',
}) {
  return {
    type: SEND_NOTIFICATION,
    payload: { type, message, title },
  };
}

export function doUpdateLoading(isLoading) {
  return {
    type: UPDATE_APP_LOADING,
    isLoading,
  };
}

export function doWaitForAnimate(payload) {
  return {
    type: WAIT_FOR_ANIMATE,
    payload,
  };
}
