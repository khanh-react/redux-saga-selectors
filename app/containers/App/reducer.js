/* eslint-disable no-param-reassign */
/* eslint-disable no-case-declarations */
import produce from 'immer';
import { NotificationManager } from 'react-notifications';

import { SEND_NOTIFICATION, UPDATE_APP_LOADING } from './constants';

const initialState = {
  currentUser: null,
  currentMessage: null,
  notification: {
    type: null,
    message: null,
  },
  isLoading: false,
};

export const AppReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SEND_NOTIFICATION:
        const { type, message } = action.payload;
        if (type === 'SUCCESS') {
          NotificationManager.success(message);
          draft.currentMessage = message;
        }
        break;

      case UPDATE_APP_LOADING:
        draft.isLoading = action.isLoading;
        break;

      default:
        break;
    }
  });

export default AppReducer;
