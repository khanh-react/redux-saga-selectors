import Loading from 'components/Loading';
import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import OthersPage from 'containers/OthersPage/Loadable';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import { NotificationContainer } from 'react-notifications';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import GlobalStyle from '../../global-styles';
import Footer from './components/Footer';
import Header from './components/Header';
import appReducer from './reducer';
import appSaga from './saga';
import { makeSelectLoading } from './selectors';

const key = 'global';
export const App = ({ isLoading }) => {
  useInjectReducer({ key, reducer: appReducer });
  useInjectSaga({ key, saga: appSaga });

  return (
    <React.Fragment>
      <Header />
      <NotificationContainer />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/others" component={OthersPage} />
        <Route component={NotFoundPage} />
      </Switch>
      <Loading isLoading={isLoading} />
      <Footer />
      <GlobalStyle />
    </React.Fragment>
  );
};

const mapStateToProps = createStructuredSelector({
  isLoading: makeSelectLoading(),
});

const mapDispatchToProps = () => ({});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

App.propTypes = {
  isLoading: PropTypes.bool,
};

export default compose(
  withConnect,
  memo,
)(App);
