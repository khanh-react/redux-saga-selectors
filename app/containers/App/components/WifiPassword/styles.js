import styled from 'styled-components';

const styles = styled.div`
  font-style: italic;
  overflow-wrap: break-word;
  color: #06dcf9;
  text-align: center;
  font-size: 11px;
  letter-spacing: normal;
  font-weight: normal;
  margin: 10px;
`;

export default styles;
