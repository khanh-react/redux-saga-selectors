import React from 'react';

import WifiPasswordStyles from './styles';

const WifiPassword = () => (
  <WifiPasswordStyles className="wifi-password">
    (Wifi password: xincamon)
  </WifiPasswordStyles>
);

export default WifiPassword;
