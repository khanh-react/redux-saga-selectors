import AppLogo from 'images/app-logo.png';
import React from 'react';

import HeaderStyles from './styles';

export function Header() {
  return (
    <HeaderStyles id="navbar" className="container-fluid">
      <div className="navbar-wrap">
        <div
          className="navbar-logo"
          onClick={() => (window.location.href = '/')}
        >
          <img src={AppLogo} alt="" />
        </div>
        <div className="navbar-branch">
          <span className="brand">Trình Coffee</span>
          <span className="sub-brand">134 Phan Châu Trinh, Đà Nẵng</span>
        </div>
      </div>
    </HeaderStyles>
  );
}

export default Header;
