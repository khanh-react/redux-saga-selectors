import styled from 'styled-components';

const styles = styled.nav`
  width: 100%;
  height: 50px;
  border-bottom: 1px solid sivler;
  box-shadow: 0px 2px 2px 0px rgba(153, 153, 153, 0.2);
  display: flex;
  position: fixed;
  top: 0;
  background-color: #ffffff;
  border: 1px solid #06dcf9;
  z-index: 100;

  .navbar-wrap {
    width: 100%;
    position: relative;
  }

  .navbar-logo {
    width: 60px;
    height: 100%;
    display: flex;
    align-items: center;
    position: absolute;
    z-index: 1;
  }

  .navbar-branch {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: absolute;
    .sub-brand {
      font-size: x-small;
      font-weight: 600;
      opacity: 0.6;
    }
  }

  .navbar-branch span{
    font-size: small;
    font-weight: 900;
  }

  .navbar-wrap img {
    width: 100%;
  }

  @media only screen and (max-width: 600px) {
    .navbar-branch {
      margin-left: 0px;
    }
  }
}
`;

export default styles;
