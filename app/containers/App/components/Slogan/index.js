import propTypes from 'prop-types';
import React from 'react';

import SloganStyles from './styles';

export const Slogan = ({ slogan }) => (
  <SloganStyles className="slogan">{slogan}</SloganStyles>
);

Slogan.propTypes = {
  slogan: propTypes.string,
}

export default Slogan;
