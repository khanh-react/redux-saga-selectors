import styled from 'styled-components';

const styles = styled.div`
  line-height: 1.2;
  font-weight: 500;
  font-size: 14px;
  margin-bottom: 20px;
  font-family: inherit;
  font-size: 11px;
  letter-spacing: normal;
  font-weight: normal;
  font-style: normal;
  text-decoration: none;
  z-index: 910;
  pointer-events: all;
  text-align: center;
  text-transform: uppercase;
`;

export default styles;
