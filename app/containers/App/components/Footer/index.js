import PropTypes from 'prop-types';
import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import FooterStyles from './styles';

const Footer = ({ location }) => {
  const { pathname } = location;
  return (
    <FooterStyles>
      {pathname.includes('others') ? (
        <Link className="evaluate-others" to="/">
          Quay lại
        </Link>
      ) : (
        <Link className="evaluate-others" to="/others">
          Đánh giá vấn đề khác
        </Link>
      )}
    </FooterStyles>
  );
};

Footer.propTypes = {
  location: PropTypes.object,
};

export default withRouter(props => <Footer {...props} />);
