import styled from 'styled-components';

const styles = styled.footer`
  position: fixed;
  bottom: 0;
  width: 100%;
  height: 40px;
  padding: 6px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 3px;
  .evaluate-others {
    height: 100%;
    border-radius: 20px;
    display: flex;
    align-items: center;
    padding: 0px 10px;
    text-transform: uppercase;
    font-size: 12px;
    font-weight: 600;
    box-shadow: 2px 2px 5px 2px silver;
    background: #ffffff;
  }
`;
export default styles;
