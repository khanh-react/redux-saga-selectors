import { delay, put, takeLatest } from 'redux-saga/effects';

import { doUpdateLoading } from './actions';
import { WAIT_FOR_ANIMATE } from './constants';

export function* setWaitForAnimate() {
  yield put(doUpdateLoading(true));
  yield delay(1000);
  yield put(doUpdateLoading(false));
  yield 123;
}

export default function* appSaga() {
  yield takeLatest(WAIT_FOR_ANIMATE, setWaitForAnimate);
}
