export const OTHER_PAGE_SLOGAN =
  'Hãy giúp chúng tôi đánh giá các hạng mục khác của cửa hàng. Để nâng cao chất lượng phục vụ bạn tốt hơn!';
export const UPDATE_CURRENT_CATALOG_INDEX = 'UPDATE_CURRENT_CATALOG_INDEX';
export const UPDATE_LOADING = 'UPDATE_OTHER_LOADING';
export const SUBMIT_PRE_SEND = 'SUBMIT_OTHER_PRE_SEND';
export const SUBMIT_PRE_SEND_SUCCESS = 'SUBMIT_OTHER_PRE_SEND_SUCCESS';
export const SUBMIT_SEND_WITH_INFO = 'SUBMIT_OTHER_SEND_WITH_INFO';
export const SUBMIT_SEND_WITH_INFO_SUCCESS = 'SUBMIT_OTHER_SEND_WITH_INFO_SUCCESS';
export const SUBMIT_SEND_WITHOUT_INFO = 'SUBMIT_OTHER_SEND_WITHOUT_INFO';
export const SUBMIT_SEND_WITHOUT_INFO_SUCCESS =
  'SUBMIT_OTHER_SEND_WITHOUT_INFO_SUCCESS';
export const UPDATE_RATING_CATALOG = 'UPDATE_RATING_CATALOG';
export const CLOSE_PRE_SEND = 'CLOSE_OTHER_PRE_SEND';
