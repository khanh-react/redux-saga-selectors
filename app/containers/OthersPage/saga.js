import { delay, put, takeLatest } from 'redux-saga/effects';

import { doSendNotification, doUpdateLoading } from '../App/actions';
import { doSendWithInfoSuccess, doSendWithOutInfoSuccess, doSubmitPreSendSuccess } from './actions';
import { SUBMIT_PRE_SEND, SUBMIT_SEND_WITH_INFO, SUBMIT_SEND_WITHOUT_INFO } from './contants';

export function* submitPreSend() {
  yield put(doUpdateLoading(true));
  yield delay(1000);
  yield put(doUpdateLoading(false));
  yield put(doSubmitPreSendSuccess());
}

export function* submitSendWithInfo() {
  yield put(doUpdateLoading(true));
  yield delay(1000);
  yield put(doUpdateLoading(false));
  yield put(doSendWithInfoSuccess());
  const noty = {
    type: 'SUCCESS',
    message: 'Trình Coffe chân thành cảm ơn đóng góp của quý khách !',
  };
  yield put(doSendNotification(noty));
}

export function* submitSendWithOutInfo() {
  yield put(doUpdateLoading(true));
  yield delay(1000);
  yield put(doUpdateLoading(false));
  yield put(doSendWithOutInfoSuccess());
  const noty = {
    type: 'SUCCESS',
    message: 'Trình Coffe chân thành cảm ơn đóng góp của quý khách !',
  };
  yield put(doSendNotification(noty));
}

export default function* otherSaga() {
  yield takeLatest(SUBMIT_PRE_SEND, submitPreSend);
  yield takeLatest(SUBMIT_SEND_WITH_INFO, submitSendWithInfo);
  yield takeLatest(SUBMIT_SEND_WITHOUT_INFO, submitSendWithOutInfo);
}
