import EvaluateTags from 'containers/HomePage/components/EvaluateTags';
import ImagesLetters from 'containers/HomePage/components/ImagesLetters';
import Rating from 'containers/HomePage/components/Rating';
import ArrowDown from 'images/arrow-down.png';
import ArrowUp from 'images/arrow-up.png';
import PropTypes from 'prop-types';
import React from 'react';

import CatalogStyles from './styles';

const CatalogItem = ({
  rating,
  evaluateKey,
  evaluateName,
  isActive,
  onClick,
  onGetRatingPointValue,
  onGetRatingEvaluateValue,
  onGetRatingLettersValue,
}) => (
  <CatalogStyles className="catalog" onClick={e => onClick(e)}>
    <div className={`catalog-line ${isActive ? 'active' : ''}`}>
      <span className="catalog-name">{evaluateName}</span>
      {!isActive && <img className="arrow" src={ArrowDown} alt="" />}
      {isActive && <img className="arrow" src={ArrowUp} alt="" />}
    </div>
    {isActive && (
      <div className="catalog-details">
        <Rating
          rating={rating.rating || 3}
          onChange={value => onGetRatingPointValue(value)}
        />
        <EvaluateTags
          className="eval-tags"
          evaluateKey={evaluateKey}
          isVisible
          rating={rating.rating || 3}
          onChange={value => onGetRatingEvaluateValue(value)}
        />
        <ImagesLetters
          className="images-letters"
          rating={rating.rating || 3}
          isVisible
          onLettersChange={value => onGetRatingLettersValue(value)}
        />
      </div>
    )}
  </CatalogStyles>
);

CatalogItem.propTypes = {
  rating: PropTypes.object,
  evaluateKey: PropTypes.string,
  evaluateName: PropTypes.string,
  isActive: PropTypes.bool,
  onClick: PropTypes.func,
  onGetRatingPointValue: PropTypes.func,
  onGetRatingEvaluateValue: PropTypes.func,
  onGetRatingLettersValue: PropTypes.func,
};

export default CatalogItem;
