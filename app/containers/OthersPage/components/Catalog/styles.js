import styled from 'styled-components';

const styles = styled.div`
  .catalog-line {
    position: relative;
    border-bottom: 2px solid silver;
    padding-left: 15px;
    margin-bottom: 15px;

    &.active {
      border-bottom: 2px solid #28a0f9;
    }

    .catalog-name {
      color: #28a0f9;
      font-weight: 600;
      font-size: 14px;
      text-transform: uppercase;
    }

    .arrow {
      position: absolute;
      right: 0;
      top: -5px;
    }
  }

  .catalog-details {
    margin-bottom: 20px;
    .eval-tags {
      padding-left: 0px !important;
      margin-top: 10px;
    }
    .images-letters {
      padding: 0 !important;
    }
  }
`;

export default styles;
