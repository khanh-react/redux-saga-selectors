import Button from 'components/Button';
import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';

import CatalogItem from '../Catalog';
import OthersFormStyles from './styles';

const OthersForm = ({
  catalogs,
  ratings,
  currentCatalogIndex,
  isPreSend,
  isHidden,
  onPreSend,
  onGetCurrentCatalogIndex,
  onUpdateRatingCatalog,
  // onGetRatingEvaluateValue,
}) => {
  if (isHidden) return null;
  return (
    <OthersFormStyles>
      <h5>Vui lòng chọn mục cần đánh giá</h5>
      <span className="closer">
        <Link to="/">X</Link>
      </span>

      <div className="catalogs">
        {catalogs.map((catalog, index) => (
          <CatalogItem
            key={catalog.key}
            rating={ratings[catalog.key]}
            evaluateKey={catalog.key}
            evaluateName={catalog.name}
            onClick={() => onGetCurrentCatalogIndex(index)}
            onGetRatingPointValue={value =>
              onUpdateRatingCatalog({
                catalog: catalog.key,
                key: 'rating',
                value,
              })
            }
            onGetRatingEvaluateValue={value =>
              onUpdateRatingCatalog({
                catalog: catalog.key,
                key: 'evaluate',
                value,
              })
            }
            onGetRatingLettersValue={value =>
              onUpdateRatingCatalog({
                catalog: catalog.key,
                key: 'letters',
                value,
              })
            }
            isActive={index === currentCatalogIndex}
          />
        ))}
      </div>

      <div className="row foot">
        <Button type="primary" text="Gửi" onClick={() => onPreSend(1)} />
      </div>
    </OthersFormStyles>
  );
};

OthersForm.propTypes = {
  ratings: PropTypes.object,
  catalogs: PropTypes.array,
  currentCatalogIndex: PropTypes.number,
  isPreSend: PropTypes.bool,
  isHidden: PropTypes.bool,
  onPreSend: PropTypes.func,
  onGetCurrentCatalogIndex: PropTypes.func,
  onSetRatingCatalog: PropTypes.func,
  onGetRatingPointValue: PropTypes.func,
  onUpdateRatingCatalog: PropTypes.func,
};

export default OthersForm;
