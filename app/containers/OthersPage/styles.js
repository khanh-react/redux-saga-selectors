import styled from 'styled-components';

const styles = styled.div`
  background-color: #f6f6f6;
  &.opacity {
    .wifi-password,
    .slogan {
      opacity: 0.3;
    }
  }
`;

export default styles;
