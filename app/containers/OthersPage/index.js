import PropTypes from 'prop-types';
import React, { memo, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import Slogan from '../App/components/Slogan';
import WifiPassword from '../App/components/WifiPassword';
import { doChangeContactInfos, doCheckValidation } from '../HomePage/actions';
import PreSendForm from '../HomePage/components/PreSendForm';
import { makeSelectContactFormValid, makeSelectContactInfo } from '../HomePage/selectors';
import {
  doClosePreSend,
  doSendRating,
  doSubmitPreSend,
  doUpdateCurrentCatalogIndex,
  doUpdateLoading,
  doUpdateRatingCatalog,
} from './actions';
import OthersForm from './components/OthersForm';
import { OTHER_PAGE_SLOGAN } from './contants';
import reducer from './reducer';
import saga from './saga';
import {
  makeSelectCurrentCatalog,
  makeSelectCurrentCatalogIndex,
  makeSelectLoading,
  makeSelectPreSend,
  makeSelectRatingData,
} from './selectors';
import OtherPageStyles from './styles';

const key = 'other';

const Others = ({
  ratingData,
  ratings,
  contactInfo,
  isPreSend,
  isLoading,
  isContactInfoValid,
  currentCatalogIndex,
  onClosePreSend,
  onSetCurrentCatalogIndex,
  onUpdateRatingCatalog,
  onUpdateContactInfo,
  onCheckValidation,
  onSend,
  onPreSend,
}) => {
  // eslint-disable-next-line no-console
  console.log('::: ORTHER PAGE RENDERED :::');
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  useEffect(() => {});

  return (
    <OtherPageStyles className="container container-fluid opacity">
      <WifiPassword />
      <Slogan slogan={OTHER_PAGE_SLOGAN} />
      <OthersForm
        ratings={ratings}
        catalogs={ratingData}
        currentCatalogIndex={currentCatalogIndex}
        isPreSend={isPreSend}
        onPreSend={type => onPreSend(type)}
        onGetCurrentCatalogIndex={index => onSetCurrentCatalogIndex(index)}
        onUpdateRatingCatalog={payload => onUpdateRatingCatalog(payload)}
        isHidden={isPreSend}
      />
      <PreSendForm
        isVisible={isPreSend}
        isValid={isContactInfoValid}
        item={ratingData[0]}
        contactInfo={contactInfo}
        onChange={payload =>
          onUpdateContactInfo(payload) && onCheckValidation()
        }
        onSend={type => onSend(type)}
        onClose={() => onClosePreSend()}
      />
    </OtherPageStyles>
  );
};

Others.propTypes = {
  ratingData: PropTypes.array,
  ratings: PropTypes.object,
  contactInfo: PropTypes.object,
  currentCatalogIndex: PropTypes.number,
  isPreSend: PropTypes.bool,
  isLoading: PropTypes.bool,
  isContactInfoValid: PropTypes.bool,
  onPreSend: PropTypes.func,
  onClosePreSend: PropTypes.func,
  // onGetCurrentCatalog: PropTypes.func,
  // onSetCurrentCatalog: PropTypes.func,
  // onGetRatingPointValue: PropTypes.func,
  onSetCurrentCatalogIndex: PropTypes.func,
  onUpdateRatingCatalog: PropTypes.func,
  onUpdateContactInfo: PropTypes.func,
  onCheckValidation: PropTypes.func,
  onSend: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  ratingData: makeSelectRatingData(),
  ratings: makeSelectCurrentCatalog(),
  contactInfo: makeSelectContactInfo(),
  isPreSend: makeSelectPreSend(),
  isLoading: makeSelectLoading(),
  isContactInfoValid: makeSelectContactFormValid(),
  currentCatalogIndex: makeSelectCurrentCatalogIndex(),
});

const mapDispatchToProps = dispatch => ({
  onSetLoading: isLoading => dispatch(doUpdateLoading(isLoading)),
  onClosePreSend: () => dispatch(doClosePreSend()),
  onSetCurrentCatalogIndex: index =>
    dispatch(doUpdateCurrentCatalogIndex(index)),
  onUpdateRatingCatalog: payload => dispatch(doUpdateRatingCatalog(payload)),
  onUpdateContactInfo: payload => dispatch(doChangeContactInfos(payload)),
  onCheckValidation: () => dispatch(doCheckValidation()),
  onPreSend: () => dispatch(doSubmitPreSend()),
  onSend: type => dispatch(doSendRating(type)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Others);
