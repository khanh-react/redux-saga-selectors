import { createSelector } from 'reselect';

import { initialState } from './reducer';

const selectOtherState = state => state.other || initialState;

const makeSelectRatingData = () =>
  createSelector(
    selectOtherState,
    state => state.ratingData,
  );

const makeSelectPreSend = () =>
  createSelector(
    selectOtherState,
    state => state.isPreSend,
  );

const makeSelectLoading = () =>
  createSelector(
    selectOtherState,
    state => state.isLoading,
  );

const makeSelectCurrentCatalogIndex = () =>
  createSelector(
    selectOtherState,
    state => state.currentCatalogIndex,
  );

const makeSelectCurrentCatalog = () =>
  createSelector(
    selectOtherState,
    state => state.ratings,
  );

export {
  makeSelectRatingData,
  makeSelectPreSend,
  makeSelectLoading,
  makeSelectCurrentCatalogIndex,
  makeSelectCurrentCatalog,
};

