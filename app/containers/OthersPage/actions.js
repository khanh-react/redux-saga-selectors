// import { SUBMIT_SEND_WITH_INFO_SUCCESS, SUBMIT_SEND_WITHOUT_INFO_SUCCESS } from '../HomePage/constants';
import {
  CLOSE_PRE_SEND,
  SUBMIT_PRE_SEND,
  SUBMIT_PRE_SEND_SUCCESS,
  SUBMIT_SEND_WITH_INFO,
  SUBMIT_SEND_WITH_INFO_SUCCESS,
  SUBMIT_SEND_WITHOUT_INFO_SUCCESS,
  UPDATE_CURRENT_CATALOG_INDEX,
  UPDATE_LOADING,
  UPDATE_RATING_CATALOG,
} from './contants';

export function doUpdateCurrentCatalogIndex(index) {
  return {
    type: UPDATE_CURRENT_CATALOG_INDEX,
    index,
  };
}

export function doUpdateLoading(isLoading) {
  return {
    type: UPDATE_LOADING,
    isLoading,
  };
}

export function doSubmitPreSend() {
  return {
    type: SUBMIT_PRE_SEND,
  };
}

export function doSubmitPreSendSuccess() {
  return {
    type: SUBMIT_PRE_SEND_SUCCESS,
  };
}

export function doUpdateRatingCatalog(payload) {
  return {
    type: UPDATE_RATING_CATALOG,
    payload,
  };
}

export function doClosePreSend() {
  return {
    type: CLOSE_PRE_SEND,
  };
}

export function doSendRating(type) {
  return {
    type: type ? SUBMIT_SEND_WITH_INFO : SUBMIT_SEND_WITH_INFO,
  };
}

export function doSendWithInfoSuccess() {
  return {
    type: SUBMIT_SEND_WITH_INFO_SUCCESS,
  };
}

export function doSendWithOutInfoSuccess() {
  return {
    type: SUBMIT_SEND_WITHOUT_INFO_SUCCESS,
  };
}
