import produce from 'immer';
import ratingData from 'static/rating_data_default.json';

import {
  CLOSE_PRE_SEND,
  SUBMIT_PRE_SEND_SUCCESS,
  SUBMIT_SEND_WITH_INFO_SUCCESS,
  SUBMIT_SEND_WITHOUT_INFO_SUCCESS,
  UPDATE_CURRENT_CATALOG_INDEX,
  UPDATE_LOADING,
  UPDATE_RATING_CATALOG,
} from './contants';

export const initialState = {
  ratingData,
  isPreSend: false,
  isLoading: false,
  ratings: {
    STAFF: {
      rating: null,
      letters: null,
      images: [],
    },
    SPACE: {
      rating: null,
      letters: null,
      images: [],
    },
    FOOD: {
      rating: null,
      letters: null,
      images: [],
    },
    DRINK: {
      rating: null,
      letters: null,
      images: [],
    },
  },
  currentCatalogIndex: null,
};

/* eslint-disable default-case, no-param-reassign */
const OtherPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_CURRENT_CATALOG_INDEX:
        draft.currentCatalogIndex = action.index;
        break;
      case UPDATE_LOADING:
        draft.isLoading = action.isLoading;
        break;
      case SUBMIT_PRE_SEND_SUCCESS:
        draft.isPreSend = true;
        // draft.isLoading = false;
        break;
      case CLOSE_PRE_SEND:
        draft.isPreSend = false;
        break;
      case UPDATE_RATING_CATALOG:
        // eslint-disable-next-line no-case-declarations
        const { catalog, key, value } = action.payload;
        draft.ratings[catalog][key] = value;
        break;
      case SUBMIT_SEND_WITH_INFO_SUCCESS:
      case SUBMIT_SEND_WITHOUT_INFO_SUCCESS:
        draft.isPreSend = false;
        draft.currentCatalogIndex = null;
        break;
      default:
        break;
    }
  });

export default OtherPageReducer;
