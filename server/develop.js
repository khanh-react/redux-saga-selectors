/* eslint consistent-return:0 import/order:0 */

const express = require('express');
const logger = require('./logger');
const argv = require('./argv');
const cors = require('cors');
const path = require('path');
const isDev = process.env.NODE_ENV !== 'production';

const ngrok =
  (isDev && process.env.ENABLE_TUNNEL) || argv.tunnel
    ? require('ngrok')
    : false;
const app = express();

app.use(cors());

app.use('/images', express.static(path.join(__dirname, './images')));

app.use('/static', express.static(path.join(__dirname, './static')));

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

app.get('/api/employees', async (req, res) => {
  const data = [
    {
      id: 1,
      name: 'Nguyễn Quang Khánh',
      avatar: '/images/sample-avatar.png',
      role: 'Phục vụ',
      ca: '7:00 - 12:00',
      status: 'online',
    },
    {
      id: 2,
      name: 'Lê Quốc Hùng',
      avatar: '/images/sample-avatar.png',
      role: 'Giữ xe',
      ca: '7:00 - 12:00',
      status: 'online',
    },
  ];
  const dataFull = [
    {
      id: 1,
      name: 'Nguyễn Quang Khánh',
      avatar: '/images/sample-avatar.png',
      role: 'Phục vụ',
      ca: '7:00 - 12:00',
      status: 'online',
    },
    {
      id: 2,
      name: 'Lê Quốc Hùng',
      avatar: '/images/sample-avatar.png',
      role: 'Giữ xe',
      ca: '7:00 - 12:00',
      status: 'online',
    },
    {
      id: 3,
      name: 'Trần Thị Thu Hoài',
      avatar: '/images/sample-avatar.png',
      role: 'Đầu bếp',
      ca: '13:00 - 17:00',
      status: 'offline',
    },
    {
      id: 4,
      name: 'Đỗ Thủ Khoa',
      avatar: '/images/sample-avatar.png',
      role: 'Phục vụ',
      ca: '13:00 - 17:00',
      status: 'offline',
    },
    {
      id: 5,
      name: 'Ngô Quốc Khang Hy',
      avatar: '/images/sample-avatar.png',
      role: 'Phục vụ',
      ca: '13:00 - 17:00',
      status: 'offline',
    },
    {
      id: 6,
      name: 'Không nhớ rõ',
      avatar: '/images/sample-avatar.png',
      role: '',
      ca: '',
      status: 'offline',
    },
  ];
  if (req.query && req.query.shift === 'all') {
    await timeout(3000);
    return res.json(dataFull);
  }
  return res.json(data);
});

// get the intended host and port number, use localhost and port 3000 if not provided
const customHost = argv.host || process.env.HOST;
const host = customHost || null; // Let http.Server use its default IPv6/4 host
const prettyHost = customHost || 'localhost';

// Start your app.
app.listen(8000, host, async err => {
  if (err) {
    return logger.error(err.message);
  }

  // Connect to ngrok in dev mode
  if (ngrok) {
    let url;
    try {
      url = await ngrok.connect(8000);
    } catch (e) {
      return logger.error(e);
    }
    logger.appStarted(8000, prettyHost, url);
  } else {
    logger.appStarted(8000, prettyHost);
  }
});
